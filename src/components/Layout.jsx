import NavBar from "@/components/Navbar"
import { useState } from "react"


export default function Layout({children}) {
  const [showNav, setShowNav] = useState(false)
  return(
    <>
    <div className="bg-black min-h-screen">
    <div className="block md:hidden flex items-center p-4">
      <button onClick={()=>setShowNav(true)}>
      <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 text-white h-6">
        <path strokeLinecap="round" strokeLinejoin="round" d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5" />
      </svg>
      </button>
    </div>
    <div className=" flex">
      <NavBar show={showNav}/>
      <div className="bg-white h-screen overflow-auto flex-grow mt-2 mr-2 mb-2 rounded-lg p-4">{children}</div>
    </div>
    </div>
    </>
  )
}
