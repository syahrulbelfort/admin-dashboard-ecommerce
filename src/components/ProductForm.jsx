import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useRouter } from 'next/router';
import Spinner from './Spinner';
import { ReactSortable } from 'react-sortablejs';

export default function ProductForm({
    _id,
    title: existingTitle,
    description: existingDesc,
    price: existingPrice,
    images : existingImages,
    category : assginedCategory,
    properties : assignedProperties
}) {
    const [title, setTitle] = useState( existingTitle || '');
    const [description, setDescription] = useState(existingDesc || '');
    const [price, setPrice] = useState(existingPrice || '');
    const [images, setImages] = useState(existingImages || []);
    const [category, setCategory] = useState(assginedCategory || '')
    const [goToProduct, setGoToProduct] = useState(false);
    const [isUploading, setIsuploading] = useState(false)
    const [categories, setCategories] = useState([])
    const [productProperties,setProductProperties] = useState(assignedProperties || {});

    const router = useRouter();

    useEffect(()=>{
        fetchCategories()
    },[])

    const fetchCategories  = async ()=>{
        const response = await axios.get('/api/categories')
        setCategories(response.data)
    }

    const savedProduct = async (e) => {
        e.preventDefault();
        const data = { title, description, price,images, category,properties:productProperties
        };
        try {
            if(_id){
                //update product
                await axios.put('/api/products', {...data,_id});
            } else {
                //create product
                await axios.post('/api/products', data);
               console.log('Product Created');
            }
            setGoToProduct(true)
        } catch (error) {
          console.error('Error creating product:', error);
        }
      };
      if(goToProduct){
        router.push('/products')
      }

      const uploadImages = async (e)=>{
        const files = e.target?.files;
        if(files?.length > 0){
            setIsuploading(true)
            const data = new FormData();
            console.log(data);
            for(const file of files){
                data.append('file',file)
            }
            const response = await axios.post('/api/upload', data)
            setImages(oldImage => {
                console.log(oldImage);
                return [...oldImage, ...response.data.links]
            } )
        }
        setIsuploading(false)
         }

    const updateImagesOrder = (images) =>{
        setImages(images);
    }


    // properties management
    function setProductProp(propName,value) {
        setProductProperties(prev => {
          const newProductProps = {...prev};
          newProductProps[propName] = value;
          return newProductProps;
        });
      }

    const propertiesToFill = [];
    if (categories.length > 0 && category) {
      let catInfo = categories.find(({_id}) => _id === category);
      propertiesToFill.push(...catInfo.properties);
      while(catInfo?.parent?._id) {
        const parentCat = categories.find(({_id}) => _id === catInfo?.parent?._id);
        propertiesToFill.push(...parentCat.properties);
        catInfo = parentCat;
      }
    }
// properties management

  return (
    <>
    <form onSubmit={savedProduct}> 
    <label>Produt name</label>
    <input 
    onChange={(e)=> setTitle(e.target.value)} 
    placeholder='product name' 
    type='text' 
    value={title}/>
    <label>Category</label>
    <select value={category} onChange={e => setCategory(e.target.value)}>
        <option value=''>Uncategories</option>
        {categories?.map((category)=>(
            <option key={category._id} value={category._id}>{category.name}</option>
        ))}
      </select>
      {propertiesToFill.length > 0 && propertiesToFill.map(p => (
          <div key={p.name} className="">
            <label>{p.name[0].toUpperCase()+p.name.substring(1)}</label>
            <div>
              <select value={productProperties[p.name]}
                      onChange={ev =>
                        setProductProp(p.name,ev.target.value)
                      }
              >
                {p.values.map(v => (
                  <option key={v} value={v}>{v}</option>
                ))}
              </select>
            </div>
          </div>
        ))}
    <label>Photos</label>
    <div className='mb-2 flex flex-wrap gap-1'>
    <ReactSortable 
    list={images}
    className='flex flex-wrap gap-1'
    setList={updateImagesOrder}>
    {!!images?.length && images.map(link=>(
        <div key={link}>
            <img className='h-24 rounded-lg' src={link}/>
            
        </div>
    ))}
    </ReactSortable>
    {isUploading && (
        <div className='rounded-lg flex items-center'> <Spinner/></div>
    )}
        <label className='h-24 w-24 rounded-md cursor-pointer bg-gray-100 border flex justify-center items-center '><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="text-gray-400 w-6 h-6">
            <path strokeLinecap="round" strokeLinejoin="round" d="M3 16.5v2.25A2.25 2.25 0 005.25 21h13.5A2.25 2.25 0 0021 18.75V16.5m-13.5-9L12 3m0 0l4.5 4.5M12 3v13.5" />
            </svg> 
        <span className='text-sm text-gray-400'>Upload</span>
        <input 
        type='file' 
        onChange={uploadImages} 
        className='hidden'/>
        </label>
        {images.length === 0 && (
            <div>No photos in this product</div>
        )}
    </div>
    <label>Description</label>
    <textarea 
    onChange={(e)=> setDescription(e.target.value)} 
    placeholder='Description' 
    type='text' 
    value={description}/>
    <label>Price (In IDR)</label>
    <input 
    onChange={(e)=> setPrice(e.target.value)} 
    placeholder='price' 
    type='number' 
    value={price}/>
    <button className='btn-primary mt-4'>Save</button>
    </form>
</>
)  
}
