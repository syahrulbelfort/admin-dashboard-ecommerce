import mongoose from "mongoose";

const { Schema, model, models } = mongoose;

const orderSchema = new Schema({
    line_items : Object,
    name: String,
    email: String,
    city : String,
    postalCode: String,
    streetAddress : String,
    country: String,
    paid: Boolean,
},{
    timestamps : true,
})

export const Order = models?.Order || model('Order', orderSchema)