import NextAuth, { getServerSession } from "next-auth"
import GoogleProvider from "next-auth/providers/google"
import clientPromise from "@/libs/mongoDB"
import { MongoDBAdapter } from "@auth/mongodb-adapter"

const adminEmail = ['syahrulbelfort@gmail.com']

export const authOptions = {
  // Configure one or more authentication providers
  providers: [
    GoogleProvider({
      clientId: process.env.GOOGLE_ID,
      clientSecret: process.env.GOOGLE_SECRET,
    }),
    // ...add more providers here
  ],
  adapter: MongoDBAdapter(clientPromise),
  callbacks:{
    session : ({session, token, user})=>{
      if(adminEmail.includes(session?.user?.email)){
        return session
      } else {
        return false
      }
    }
  }
}

export default NextAuth(authOptions)

export const isAdminReq = async (req,res) =>{
  const session = await getServerSession(req,res, authOptions);
  if(!adminEmail.includes(session?.user?.email)){
    res.status(401)
    res.end()
    throw 'not an admin'
  }
}