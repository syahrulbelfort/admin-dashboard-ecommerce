import { MongooseConnect } from "@/libs/mongoose";
import {Category} from "@/models/categories";

export default async function handle(req, res) {
  const { method } = req;
  await MongooseConnect();

  if (method === 'GET') {
    const categories = await Category.find().populate('parent');
    res.json(categories);
  }
  
  if (method === 'POST') {
    const {name,parentCategory,properties} = req.body;
    const categoryDoc = await Category.create({
      name,
      parent: parentCategory || undefined,
      properties,
    });
    res.json(categoryDoc);
  }

  if (method === 'PUT') {
    const {name,parentCategory,properties,_id} = req.body;
    const categoryDoc = await Category.updateOne({_id},{
      name,
      parent: parentCategory || undefined,
      properties,
    });
    res.json(categoryDoc);
  }

 if(method === 'DELETE'){
  if(req.query?._id){
    await Category.deleteOne({_id : req.query?._id})
  }
  res.json(true)
 }

  
}
