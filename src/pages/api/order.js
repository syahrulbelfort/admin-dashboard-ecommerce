import { MongooseConnect } from "@/libs/mongoose";
import {Order} from "@/models/order";

export default async function handler(req,res) {
    await MongooseConnect();
    res.json(await Order.find().sort({createdAt : -1}))

}
