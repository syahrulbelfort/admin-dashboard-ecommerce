import { MongooseConnect } from "@/libs/mongoose";
import { Product } from "@/models/product";

export default async function handle(req,res){
   const {method} = req;

   await MongooseConnect();

   if(method === 'GET'){
      const reqId = req.query?.id
      if(reqId){
         const response = await Product.findOne({_id: req.query.id})
         res.json(response)
      } else{
         const response =  await Product.find();
         res.json(response)
      }
   }

   if(method ==='POST'){
      const {title, description, price,images,category} = req.body
      const productDoc  = await Product.create({
         title,description,price,images,category
      })
      res.json(productDoc)
   }

   if(method === 'PUT'){
      const {_id, title, description, price, images,category} = req.body
      const updateProduct = await Product.updateOne(
         {_id}, {title, description,price,images, category}
      )
      res.json(updateProduct)
   }

   if(method === 'DELETE'){
     if(req.query?.id){
      await Product.deleteOne({_id : req.query?.id });
      res.json(true)
     }
   }
}