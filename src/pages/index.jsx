import Layout from "@/components/Layout"
import { useSession, signIn, signOut } from "next-auth/react"



export default function Home() {
  const { data: session } = useSession()

  if(!session){ 
    return (
      <div className="bg-blue-900 w-screen h-screen flex items-center">
        <div className="text-center w-full">
          <button onClick={() => signIn('google')} className="bg-white rounded-lg px-4 p-2">Login with Google</button>
        </div>
      </div>
    )
  }

  
return(
  <Layout>
    <div className="text-blue-900 justify-between flex gap-2">
      <h2> Hello, {session?.user?.name}</h2>
    <div className="flex bg-gray-300 text-black gap-2 rounded-lg overflow-hidden">
      <img className="w-6 h-6 " src={session?.user?.image} alt="user"/>
      <span className="px-2">  {session.user.name}</span>

    </div>
    </div>

  </Layout>
)
}
