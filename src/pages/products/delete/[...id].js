import Layout from '@/components/Layout'
import axios from 'axios'
import { useRouter } from 'next/router'
import React,{useEffect, useState} from 'react'

export default function DeleteProduct() {
    const [ productInfo, setProductInfo] = useState('')
    const router = useRouter()
    const {id} = router.query

    const getOneProduct = async () =>{
       const response =  await axios.get(`/api/products?id=${id}`)
       setProductInfo(response.data)
    }

    useEffect(()=>{
        if(!id) return
        getOneProduct()
    },[id])

    const goBack = () =>{
        router.push('/products')
    }

    const deleteProduct = async () =>{
        await axios.delete(`/api/products?id=${id}`)
        goBack()
    }

  return (
    <Layout>
        <h1 className='text-center'>Do you really want to delete "{productInfo?.title}" ?</h1>
        <div className='flex justify-center gap-2'>
        <button className='btn-delete' onClick={deleteProduct}>Yes</button>
        <button className='btn-default' onClick={goBack}>No</button>
        </div>
    </Layout>

  )
}
