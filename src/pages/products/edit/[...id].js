import Layout from '@/components/Layout'
import ProductForm from '@/components/ProductForm'
import axios from 'axios'
import { useRouter } from 'next/router'
import React, { useEffect,useState } from 'react'

export default function EditProduct() {
    const [ productInfo, setProductInfo] = useState(null)
    const router = useRouter();
    const { id } = router.query;

    const getOneId = async () =>{
      const response =  await axios.get(`/api/products?id=${id}`);
      setProductInfo(response.data);
    }

    useEffect(()=>{
        if(!id) return
        getOneId()
    },[id])
    
  return (
    <Layout>
        <h1>Edit Product</h1>
        {
            productInfo && (
                <ProductForm {...productInfo}/>
            )
        }
       
    </Layout>
  )
}
